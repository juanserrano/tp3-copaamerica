package laCopa;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		ArrayList<Equipo> g1 = new ArrayList<Equipo>();
		ArrayList<Equipo> g2 = new ArrayList<Equipo>();
		ArrayList<Equipo> g3 = new ArrayList<Equipo>();
		
		crearGrupos(g1, g2, g3);
		
		Grupo grupo1 = new Grupo("g1", g1);
		Grupo grupo2 = new Grupo("g2", g2);
		Grupo grupo3 = new Grupo("g3", g3);
		
		ArrayList<Grupo> superG = new ArrayList<Grupo>();
		superG.add(grupo1);
		superG.add(grupo2);
		superG.add(grupo3);
		
		ArrayList<Grupo> gCandidato = new ArrayList<Grupo>();
        gCandidato.clear();
        for (int i = 0; i < superG.size(); i++) {
            gCandidato.add(new Grupo(superG.get(i).getNombre(), superG.get(i).getListaEquipos()));
        }
		
        ordenarGrupos(superG, gCandidato);
        superG = copiarArray(gCandidato);
        mejorarRespuesta(superG, gCandidato);
        
        superG = copiarArray(gCandidato);
        mejorarRespuesta(superG, gCandidato);

	}
	
	private static void mejorarRespuesta(ArrayList<Grupo> superG, ArrayList<Grupo> candidato) {
		System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		for(Grupo grupoActual : superG) {
			for(Grupo grupoPermutar : superG) {
				if(grupoActual != grupoPermutar) {
					for(int i=0; i<grupoActual.getListaEquipos().size(); i++) {
						for(int t=0; t<grupoActual.getListaEquipos().size(); t++) {
							ArrayList<Grupo> gTemp = copiarArray(superG);
							
							Equipo temp = new Equipo(grupoActual.getListaEquipos().get(i).getNombre(), grupoActual.getListaEquipos().get(i).getFuerza());
							
							gTemp.get(superG.indexOf(grupoActual)).getListaEquipos().set(i, grupoPermutar.getListaEquipos().get(t));
							gTemp.get(superG.indexOf(grupoPermutar)).getListaEquipos().set(t, temp);
							
							////////////////////Estado-del-algoritmo///////////////////////////
							candidato.get(0).printGrupo();
							candidato.get(1).printGrupo();
							candidato.get(2).printGrupo();
							System.out.println();
							gTemp.get(0).printGrupo();
							gTemp.get(1).printGrupo();
							gTemp.get(2).printGrupo();
							System.out.println();
							
							System.out.print("temporal: ");
							System.out.print(gTemp.get(0).dispersionGrupo()+"/");
							System.out.print(gTemp.get(1).dispersionGrupo()+"/");
							System.out.println(gTemp.get(2).dispersionGrupo());
							
							System.out.print("candidato: ");
							System.out.print(candidato.get(0).dispersionGrupo()+"/");
							System.out.print(candidato.get(1).dispersionGrupo()+"/");
							System.out.println(candidato.get(2).dispersionGrupo());
							System.out.println();
							//////////////////////////////////////////////////////
							
							if(MinDispersionTotal(gTemp) > MinDispersionTotal(candidato)){
								System.out.println(MinDispersionTotal(gTemp) + " mayor a "+ MinDispersionTotal(candidato));
								candidato.clear();
								candidato.addAll(gTemp);
							}
							if(MinDispersionTotal(gTemp) == MinDispersionTotal(candidato)){
								if(promedioDispersion(gTemp) < promedioDispersion(candidato))
								System.out.println("paso");
								candidato.clear();
								candidato.addAll(gTemp);
							}
						}
					}
				}
			}
		}
	}

	private static void ordenarGrupos(ArrayList<Grupo> superG, ArrayList<Grupo> gCandidato) {
		for(Grupo grupoActual : superG) {
			for(Grupo grupoPermutar : superG) {
				if(grupoActual != grupoPermutar) {
					for(int i=0; i<grupoActual.getListaEquipos().size(); i++) {
						
						ArrayList<Grupo> gTemp = copiarArray(superG);
						
						Equipo temp = new Equipo(grupoActual.getListaEquipos().get(0).getNombre(), grupoActual.getListaEquipos().get(0).getFuerza());
						
						gTemp.get(superG.indexOf(grupoActual)).getListaEquipos().set(0, grupoPermutar.getListaEquipos().get(i));
						gTemp.get(superG.indexOf(grupoPermutar)).getListaEquipos().set(i, temp);
						
						////////////////////Estado-del-algoritmo///////////////////////////
						gCandidato.get(0).printGrupo();
						gCandidato.get(1).printGrupo();
						gCandidato.get(2).printGrupo();
						System.out.println();
						gTemp.get(0).printGrupo();
						gTemp.get(1).printGrupo();
						gTemp.get(2).printGrupo();
						System.out.println();
						
						System.out.print("temporal: ");
						System.out.print(gTemp.get(0).dispersionGrupo()+"/");
						System.out.print(gTemp.get(1).dispersionGrupo()+"/");
						System.out.println(gTemp.get(2).dispersionGrupo());
						
						System.out.print("candidato: ");
						System.out.print(gCandidato.get(0).dispersionGrupo()+"/");
						System.out.print(gCandidato.get(1).dispersionGrupo()+"/");
						System.out.println(gCandidato.get(2).dispersionGrupo());
						System.out.println();
						//////////////////////////////////////////////////////
						
						if(MinDispersionTotal(gTemp) > MinDispersionTotal(gCandidato)){
							System.out.println(MinDispersionTotal(gTemp) + " mayor a "+ MinDispersionTotal(gCandidato));
							gCandidato.clear();
							gCandidato.addAll(gTemp);
						}
						if(MinDispersionTotal(gTemp) == MinDispersionTotal(gCandidato)){
							if(promedioDispersion(gTemp) < promedioDispersion(gCandidato))
							System.out.println("paso");
							gCandidato.clear();
							gCandidato.addAll(gTemp);
						}
					}
				}
			}
		}
	}

	private static double promedioDispersion(ArrayList<Grupo> grupos) {
		double dispersion1 = grupos.get(0).dispersionGrupo();
		double dispersion2 = grupos.get(1).dispersionGrupo();
		double dispersion3 = grupos.get(2).dispersionGrupo();
		
		return ((dispersion1 + dispersion2 + dispersion3)/3);
	}

	private static double MinDispersionTotal(ArrayList<Grupo> grupos) {
		double dispersion1 = grupos.get(0).dispersionGrupo();
		double dispersion2 = grupos.get(1).dispersionGrupo();
		double dispersion3 = grupos.get(2).dispersionGrupo();
		
		return Math.min(dispersion1, Math.min(dispersion2, dispersion3));
	}
	
	private static ArrayList<Grupo> copiarArray(ArrayList<Grupo> grupos){
		ArrayList<Equipo> g1t = new ArrayList<Equipo>();
		ArrayList<Equipo> g2t = new ArrayList<Equipo>();
		ArrayList<Equipo> g3t = new ArrayList<Equipo>();
		
		for(Equipo equipo : grupos.get(0).getListaEquipos()) {
			g1t.add(new Equipo(equipo.getNombre(), equipo.getFuerza()));
		}
		for(Equipo equipo : grupos.get(1).getListaEquipos()) {
			g2t.add(new Equipo(equipo.getNombre(), equipo.getFuerza()));
		}
		for(Equipo equipo : grupos.get(2).getListaEquipos()) {
			g3t.add(new Equipo(equipo.getNombre(), equipo.getFuerza()));
		}
		
		ArrayList<Grupo> temporal = new ArrayList<Grupo>();
		temporal.add(new Grupo("g1q", g1t));
		temporal.add(new Grupo("g2q", g2t));
		temporal.add(new Grupo("g3q", g3t));
		return temporal;
	}

	private static void crearGrupos(ArrayList<Equipo> g1, ArrayList<Equipo>g2, ArrayList<Equipo>g3) {
		
		g1.add(new Equipo("Argentina", 41));
		g1.add(new Equipo("Bolivia", 26));
		g1.add(new Equipo("Brasil", 35));
		g1.add(new Equipo("Chile", 38));
		
		g2.add(new Equipo("Colombia", 21));
		g2.add(new Equipo("Costa Rica", 5));
		g2.add(new Equipo("Ecuador", 27));
		g2.add(new Equipo("Estados Unidos", 4));
		
		g3.add(new Equipo("Mexico", 10));
		g3.add(new Equipo("Paraguay", 36));
		g3.add(new Equipo("Peru", 31));
		g3.add(new Equipo("Venezuela", 27));
		
//		g1.add(new Equipo("Argentina", 1));
//		g1.add(new Equipo("Bolivia", 2));
//		g1.add(new Equipo("Brasil", 3));
//		g1.add(new Equipo("Chile", 4));
//		
//		g2.add(new Equipo("Colombia", 5));
//		g2.add(new Equipo("Costa Rica", 6));
//		g2.add(new Equipo("Ecuador", 7));
//		g2.add(new Equipo("Estados Unidos", 8));
//		
//		g3.add(new Equipo("Mexico", 9));
//		g3.add(new Equipo("Paraguay", 10));
//		g3.add(new Equipo("Peru", 11));
//		g3.add(new Equipo("Venezuela", 12));
		
	}

}
