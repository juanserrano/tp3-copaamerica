package laCopa;
import java.util.ArrayList;

public class Grupo {
	
	private String nombre;
	private ArrayList<Equipo> listaEquipos;
	
	public Grupo(String nombre, ArrayList<Equipo> listaEquipos) {
		setNombre(nombre);
		setListaEquipos(listaEquipos);
	}
	
	public int dispersionGrupo() {
		int cont = 0;
		if(!this.getListaEquipos().isEmpty())
		for(Equipo equipo : this.getListaEquipos()) {
			cont += ((Math.pow(equipo.getFuerza() - this.promedio(), 2))/(this.getListaEquipos().size()-1));
		}
		return cont;
	}
	
	public int promedio() {
		int cont = 0;
		for(Equipo equipo : this.getListaEquipos()) {
			cont += equipo.getFuerza();
		}
		return cont/this.getListaEquipos().size();
	}
	
	public void printGrupo() {
		System.out.print(this.getListaEquipos().get(0).getNombre() + "/" + 
				this.getListaEquipos().get(1).getNombre() + "/" +
				this.getListaEquipos().get(2).getNombre() + "/" +
				this.getListaEquipos().get(3).getNombre());
		System.out.println();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Equipo> getListaEquipos() {
		return listaEquipos;
	}

	public void setListaEquipos(ArrayList<Equipo> listaEquipos) {
		this.listaEquipos = listaEquipos;
	}

	
}
