package laCopa;

import java.util.ArrayList;

public class AlgoritmoFB {
	private long tiempoTotal;
	
	public void mejorarRespuesta(ArrayList<Grupo> superG, ArrayList<Grupo> candidato) {
		long tiempoInicial= System.currentTimeMillis();
		for(Grupo grupoActual : superG) {
			for(Grupo grupoPermutar : superG) {
				if(grupoActual != grupoPermutar) {
					for(int i=0; i<grupoActual.getListaEquipos().size(); i++) {
						for(int t=0; t<grupoActual.getListaEquipos().size(); t++) {
							ArrayList<Grupo> gTemp = copiarArray(superG);
							
							Equipo temp = new Equipo(grupoActual.getListaEquipos().get(i).getNombre(), grupoActual.getListaEquipos().get(i).getFuerza());
							
							gTemp.get(superG.indexOf(grupoActual)).getListaEquipos().set(i, grupoPermutar.getListaEquipos().get(t));
							gTemp.get(superG.indexOf(grupoPermutar)).getListaEquipos().set(t, temp);
							
							if(MinDispersionTotal(gTemp) > MinDispersionTotal(candidato)){
								System.out.println(MinDispersionTotal(gTemp) + " mayor a "+ MinDispersionTotal(candidato));
								candidato.clear();
								candidato.addAll(gTemp);
							}
							if(MinDispersionTotal(gTemp) == MinDispersionTotal(candidato)){
								if(promedioDispersion(gTemp) < promedioDispersion(candidato))
								System.out.println("paso");
								candidato.clear();
								candidato.addAll(gTemp);
							}
						}
					}
				}
			}
		}
		long tiempoFinal= System.currentTimeMillis();
		setTiempoTotal(tiempoFinal-tiempoInicial);
	}

	public void ordenarGrupos(ArrayList<Grupo> superG, ArrayList<Grupo> gCandidato) {
		long tiempoInicial= System.currentTimeMillis();
		for(Grupo grupoActual : superG) {
			for(Grupo grupoPermutar : superG) {
				if(grupoActual != grupoPermutar) {
					for(int i=0; i<grupoActual.getListaEquipos().size(); i++) {
						
						ArrayList<Grupo> gTemp = copiarArray(superG);
						
						Equipo temp = new Equipo(grupoActual.getListaEquipos().get(0).getNombre(), grupoActual.getListaEquipos().get(0).getFuerza());
						
						gTemp.get(superG.indexOf(grupoActual)).getListaEquipos().set(0, grupoPermutar.getListaEquipos().get(i));
						gTemp.get(superG.indexOf(grupoPermutar)).getListaEquipos().set(i, temp);
						
						if(MinDispersionTotal(gTemp) > MinDispersionTotal(gCandidato)){
							System.out.println(MinDispersionTotal(gTemp) + " mayor a "+ MinDispersionTotal(gCandidato));
							gCandidato.clear();
							gCandidato.addAll(gTemp);
						}
						if(MinDispersionTotal(gTemp) == MinDispersionTotal(gCandidato)){
							if(promedioDispersion(gTemp) < promedioDispersion(gCandidato))
							System.out.println("paso");
							gCandidato.clear();
							gCandidato.addAll(gTemp);
						}
					}
				}
			}
		}
		long tiempoFinal= System.currentTimeMillis();
		setTiempoTotal(tiempoFinal-tiempoInicial);
	}

	public double promedioDispersion(ArrayList<Grupo> grupos) {
		double dispersion1 = grupos.get(0).dispersionGrupo();
		double dispersion2 = grupos.get(1).dispersionGrupo();
		double dispersion3 = grupos.get(2).dispersionGrupo();
		
		return ((dispersion1 + dispersion2 + dispersion3)/3);
	}

	public double MinDispersionTotal(ArrayList<Grupo> grupos) {
		double dispersion1 = grupos.get(0).dispersionGrupo();
		double dispersion2 = grupos.get(1).dispersionGrupo();
		double dispersion3 = grupos.get(2).dispersionGrupo();
		
		return Math.min(dispersion1, Math.min(dispersion2, dispersion3));
	}
	
	public ArrayList<Grupo> copiarArray(ArrayList<Grupo> grupos){
		ArrayList<Equipo> g1t = new ArrayList<Equipo>();
		ArrayList<Equipo> g2t = new ArrayList<Equipo>();
		ArrayList<Equipo> g3t = new ArrayList<Equipo>();
		
		for(Equipo equipo : grupos.get(0).getListaEquipos()) {
			g1t.add(new Equipo(equipo.getNombre(), equipo.getFuerza()));
		}
		for(Equipo equipo : grupos.get(1).getListaEquipos()) {
			g2t.add(new Equipo(equipo.getNombre(), equipo.getFuerza()));
		}
		for(Equipo equipo : grupos.get(2).getListaEquipos()) {
			g3t.add(new Equipo(equipo.getNombre(), equipo.getFuerza()));
		}
		
		ArrayList<Grupo> temporal = new ArrayList<Grupo>();
		temporal.add(new Grupo("g1q", g1t));
		temporal.add(new Grupo("g2q", g2t));
		temporal.add(new Grupo("g3q", g3t));
		return temporal;
	}

	public long getTiempoTotal() {
		return tiempoTotal;
	}

	public void setTiempoTotal(long tiempoTotal) {
		this.tiempoTotal = tiempoTotal;
	}
}
