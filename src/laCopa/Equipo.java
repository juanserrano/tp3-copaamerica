package laCopa;

public class Equipo {
	
	private String nombre;
	private int fuerza;
	
	public Equipo(String nombre, int fuerza) {
		setNombre(nombre);
		setFuerza(fuerza);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getFuerza() {
		return fuerza;
	}

	public void setFuerza(int fuerza) {
		this.fuerza = fuerza;
	}

}
