package ventanas;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class PrimerVentana extends javax.swing.JFrame {

	private VentanaPrincipal segundaVentana;
	public PrimerVentana() {
		initialize();
	}

	private void initialize() {
		this.setTitle("La Copa Am\u00E9rica");
		this.setBounds(100, 100, 896, 304);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);

		JLabel labelSuperior = new JLabel("\u00A1 Bienvenido/a A la CONMEBOL !");
		labelSuperior.setFont(new Font("Sitka Text", Font.PLAIN, 18));
		labelSuperior.setHorizontalAlignment(SwingConstants.CENTER);
		labelSuperior.setBounds(0, 0, 880, 40);
		this.getContentPane().add(labelSuperior);

		JTextPane textoInfo = new JTextPane();
		textoInfo.setEditable(false);
		textoInfo.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		textoInfo.setText(
				"Se aproxima la Copa Am\u00E9rica y la CONMEBOL ha desarrollado una aplicaci\u00F3n para organizar la copa de la manera m\u00E1s justa y organizada posible. La Copa tendr\u00E1 la siguiente estructura: participar\u00E1n 12 equipos (selecciones) que ser\u00E1n distribuidos en 3 grupos de 4 equipos cada uno. En principio, esta distribuci\u00F3n ser\u00E1 aleatoria. Para lograr la equidad entre grupos, se ejecutar\u00E1 un algoritmo de fuerza bruta hasta llegar a la mejor repartici\u00F3n posible.");
		textoInfo.setBounds(0, 51, 880, 145);
		this.getContentPane().add(textoInfo);

		JButton botonAvanzar = new JButton("Avanzar");
		botonAvanzar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				segundaVentana= new VentanaPrincipal();
				segundaVentana.setVisible(true);
			}
		});
		botonAvanzar.setFont(new Font("Sitka Text", Font.PLAIN, 14));
		botonAvanzar.setBounds(10, 207, 106, 34);
		this.getContentPane().add(botonAvanzar);

		JButton botonSalir = new JButton("Salir");
		botonSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		botonSalir.setFont(new Font("Sitka Text", Font.PLAIN, 14));
		botonSalir.setBounds(764, 207, 106, 34);
		this.getContentPane().add(botonSalir);
	}
}
