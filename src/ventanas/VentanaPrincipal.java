package ventanas;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import javax.swing.border.LineBorder;
import java.awt.Color;


import laCopa.AlgoritmoFB;
import laCopa.Equipo;
import laCopa.Grupo;

@SuppressWarnings("serial")
public class VentanaPrincipal extends javax.swing.JFrame {
	private AlgoritmoFB alg;
	private ArrayList<Grupo> arrayGruposFinales;
	private ArrayList<Grupo> arrayGruposTemp;
	private JLabel labelGrupoBUno;
	private JLabel labelGrupoBDos;
	private JLabel labelGrupoBTres;
	private JLabel labelGrupoBCuatro;
	private JLabel labelGrupoCUno;
	private JLabel labelGrupoCDos;
	private JLabel labelGrupoCTres;
	private JLabel labelGrupoCCuatro;
	private JLabel labelGrupoAUno;
	private JLabel labelGrupoADos;
	private JLabel labelGrupoATres;
	private JLabel labelGrupoACuatro;
	private JLabel labelAlg;
	private JLabel labelInfoGrupoAequipo1;
	private JLabel labelInfoGrupoAequipo2 ;
	private JLabel labelInfoGrupoAequipo3;
	private JLabel labelInfoGrupoAequipo4;
	private JLabel labelInfoGrupoACF1;
	private JLabel labelInfoGrupoACF2;
	private JLabel labelInfoGrupoACF3;
	private JLabel labelInfoGrupoACF4;
	private JLabel labelInfoGrupoBequipo1;
	private JLabel labelInfoGrupoBequipo2; 
	private JLabel labelInfoGrupoBequipo3;
	private JLabel labelInfoGrupoBequipo4;
	private JLabel labelInfoGrupoBCF1;
	private JLabel labelInfoGrupoBCF2;
	private JLabel labelInfoGrupoBCF3;
	private JLabel labelInfoGrupoBCF4 ;
	private JLabel labelInfoGrupoCequipo1; 
	private JLabel labelInfoGrupoCequipo2 ;
	private JLabel labelInfoGrupoCequipo3;
	private JLabel labelInfoGrupoCequipo4;
	private	JLabel labelInfoGrupoCCF1;
	private JLabel labelInfoGrupoCCF2;
	private JLabel labelInfoGrupoCCF3;
	private JLabel labelInfoGrupoCCF4;
	private JLabel labelPromGrupoA;
	private JLabel labelDispGrupoA;
	private JLabel labelPromGrupoB;
	private JLabel labelDispGrupoB;
	private JLabel labelPromGrupoC;
	private JLabel labelDispGrupoC;
	private boolean primeraVez;

	public VentanaPrincipal() {
		initialize();
	}

	private void initialize() {
		setPrimeraVez(false);
		alg= new AlgoritmoFB();
		arrayGruposFinales = new ArrayList<Grupo>();
		arrayGruposTemp = new ArrayList<Grupo>();
		agregarGrupos(arrayGruposFinales);
		agregarGrupos(arrayGruposTemp);
		
		this.setTitle("La Copa Am\u00E9rica");
		this.setBounds(100, 100, 857, 704);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);

		JPanel panelSup = new JPanel();
		panelSup.setBorder(UIManager.getBorder("MenuBar.border"));
		panelSup.setBounds(0, 0, 841, 33);
		this.getContentPane().add(panelSup);
		panelSup.setLayout(null);

		JLabel labelSup = new JLabel(
				"La divisi\u00F3n de grupos que ve abajo es por defecto. para generar una repartici\u00F3n mas equitativa haga click en el boton \"Ejecutar algoritmo\"");
		labelSup.setBounds(0, 5, 841, 28);
		panelSup.add(labelSup);
		labelSup.setHorizontalAlignment(SwingConstants.CENTER);
		labelSup.setFont(new Font("Sitka Text", Font.PLAIN, 12));

		JPanel panelGrupoA = new JPanel();
		panelGrupoA.setBorder(new LineBorder(Color.BLACK, 1, true));
		panelGrupoA.setBounds(0, 44, 276, 182);
		this.getContentPane().add(panelGrupoA);
		panelGrupoA.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(Color.RED, 4, true));
		panel.setBounds(0, 0, 276, 35);
		panelGrupoA.add(panel);
		panel.setLayout(null);
		
		JLabel labelGrupoA = new JLabel("Grupo A");
		labelGrupoA.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoA.setHorizontalAlignment(SwingConstants.CENTER);
		labelGrupoA.setBounds(0, 0, 276, 35);
		panel.add(labelGrupoA);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(255, 255, 255), 3));
		panel_1.setBounds(0, 37, 276, 28);
		panelGrupoA.add(panel_1);
		panel_1.setLayout(null);
		
		labelGrupoAUno = new JLabel("New label");
		labelGrupoAUno.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoAUno.setHorizontalAlignment(SwingConstants.CENTER);
		labelGrupoAUno.setBounds(0, 0, 276, 28);
		panel_1.add(labelGrupoAUno);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(255, 255, 255), 3));
		panel_2.setBounds(0, 69, 276, 35);
		panelGrupoA.add(panel_2);
		panel_2.setLayout(null);
		
		labelGrupoADos = new JLabel("New label");
		labelGrupoADos.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoADos.setHorizontalAlignment(SwingConstants.CENTER);
		labelGrupoADos.setBounds(0, 0, 276, 30);
		panel_2.add(labelGrupoADos);
		
		JPanel panel_8 = new JPanel();
		panel_8.setBorder(new LineBorder(new Color(255, 255, 255), 3));
		panel_8.setBounds(0, 108, 276, 35);
		panelGrupoA.add(panel_8);
		panel_8.setLayout(null);
		
		labelGrupoATres = new JLabel("New label");
		labelGrupoATres.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoATres.setHorizontalAlignment(SwingConstants.CENTER);
		labelGrupoATres.setBounds(0, 0, 276, 29);
		panel_8.add(labelGrupoATres);
		
		JPanel panel_9 = new JPanel();
		panel_9.setBorder(new LineBorder(new Color(255, 255, 255), 3));
		panel_9.setBounds(0, 147, 276, 24);
		panelGrupoA.add(panel_9);
		panel_9.setLayout(null);
		
		labelGrupoACuatro = new JLabel("New label");
		labelGrupoACuatro.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoACuatro.setHorizontalAlignment(SwingConstants.CENTER);
		labelGrupoACuatro.setBounds(0, 0, 276, 24);
		panel_9.add(labelGrupoACuatro);

		JPanel panelGrupoB = new JPanel();
		panelGrupoB.setBorder(new LineBorder(Color.BLACK, 1, true));
		panelGrupoB.setBounds(286, 44, 269, 182);
		this.getContentPane().add(panelGrupoB);
		panelGrupoB.setLayout(null);

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new LineBorder(new Color(47, 79, 79), 3));
		panel_3.setBounds(0, 35, 269, 32);
		panelGrupoB.add(panel_3);
		panel_3.setLayout(null);

		labelGrupoBUno = new JLabel("New label");
		labelGrupoBUno.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoBUno.setHorizontalAlignment(SwingConstants.CENTER);
		labelGrupoBUno.setBounds(10, 5, 249, 27);
		panel_3.add(labelGrupoBUno);

		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new LineBorder(new Color(47, 79, 79), 3));
		panel_4.setBounds(0, 69, 269, 32);
		panelGrupoB.add(panel_4);
		panel_4.setLayout(null);

		labelGrupoBDos = new JLabel("New label");
		labelGrupoBDos.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoBDos.setHorizontalAlignment(SwingConstants.CENTER);
		labelGrupoBDos.setBounds(10, 5, 249, 27);
		panel_4.add(labelGrupoBDos);

		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new LineBorder(new Color(47, 79, 79), 3));
		panel_5.setBounds(0, 102, 269, 32);
		panelGrupoB.add(panel_5);
		panel_5.setLayout(null);

		labelGrupoBTres = new JLabel("New label");
		labelGrupoBTres.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoBTres.setHorizontalAlignment(SwingConstants.CENTER);
		labelGrupoBTres.setBounds(10, 5, 249, 27);
		panel_5.add(labelGrupoBTres);

		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new LineBorder(new Color(47, 79, 79), 3));
		panel_6.setBounds(0, 139, 269, 32);
		panelGrupoB.add(panel_6);
		panel_6.setLayout(null);

		labelGrupoBCuatro = new JLabel("New label");
		labelGrupoBCuatro.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoBCuatro.setHorizontalAlignment(SwingConstants.CENTER);
		labelGrupoBCuatro.setBounds(10, 5, 249, 27);
		panel_6.add(labelGrupoBCuatro);

		JPanel panel_7 = new JPanel();
		panel_7.setBorder(new LineBorder(Color.BLUE, 4, true));
		panel_7.setBounds(0, 0, 269, 32);
		panelGrupoB.add(panel_7);
		panel_7.setLayout(null);

		JLabel labelGrupoB = new JLabel("Grupo B");
		labelGrupoB.setBounds(83, 0, 106, 27);
		labelGrupoB.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoB.setHorizontalAlignment(SwingConstants.CENTER);
		panel_7.add(labelGrupoB);

		JPanel panelGrupoC = new JPanel();
		panelGrupoC.setBorder(new LineBorder(Color.BLACK, 1, true));
		panelGrupoC.setBounds(565, 44, 276, 182);
		this.getContentPane().add(panelGrupoC);
		panelGrupoC.setLayout(null);
		
		JPanel panel_10 = new JPanel();
		panel_10.setBorder(new LineBorder(Color.GRAY, 3));
		panel_10.setBounds(0, 104, 276, 31);
		panelGrupoC.add(panel_10);
		panel_10.setLayout(null);
		
		labelGrupoCTres = new JLabel("New label");
		labelGrupoCTres.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoCTres.setHorizontalAlignment(SwingConstants.CENTER);
		labelGrupoCTres.setBounds(0, 0, 276, 25);
		panel_10.add(labelGrupoCTres);
		
		JPanel panel_11 = new JPanel();
		panel_11.setBorder(new LineBorder(Color.GRAY, 3));
		panel_11.setBounds(0, 68, 276, 31);
		panelGrupoC.add(panel_11);
		panel_11.setLayout(null);
		
		labelGrupoCDos = new JLabel("New label");
		labelGrupoCDos.setHorizontalAlignment(SwingConstants.CENTER);
		labelGrupoCDos.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoCDos.setBounds(0, 0, 276, 25);
		panel_11.add(labelGrupoCDos);
		
		JPanel panel_12 = new JPanel();
		panel_12.setBorder(new LineBorder(Color.GRAY, 3));
		panel_12.setBounds(0, 36, 276, 31);
		panelGrupoC.add(panel_12);
		panel_12.setLayout(null);
		
		labelGrupoCUno = new JLabel("New label");
		labelGrupoCUno.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoCUno.setHorizontalAlignment(SwingConstants.CENTER);
		labelGrupoCUno.setBounds(0, 0, 276, 25);
		panel_12.add(labelGrupoCUno);
		
		JPanel panel_13 = new JPanel();
		panel_13.setBorder(new LineBorder(Color.GRAY, 3));
		panel_13.setBounds(0, 141, 276, 26);
		panelGrupoC.add(panel_13);
		panel_13.setLayout(null);
		
		labelGrupoCCuatro = new JLabel("New label");
		labelGrupoCCuatro.setHorizontalAlignment(SwingConstants.CENTER);
		labelGrupoCCuatro.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoCCuatro.setBounds(0, 0, 276, 25);
		panel_13.add(labelGrupoCCuatro);
		
		JPanel panel_14 = new JPanel();
		panel_14.setBorder(new LineBorder(Color.GREEN, 4));
		panel_14.setBounds(0, 0, 276, 31);
		panelGrupoC.add(panel_14);
		panel_14.setLayout(null);
		
		JLabel labelGrupoC = new JLabel("Grupo C");
		labelGrupoC.setHorizontalAlignment(SwingConstants.CENTER);
		labelGrupoC.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelGrupoC.setBounds(62, 0, 151, 25);
		panel_14.add(labelGrupoC);
		
		labelAlg = new JLabel("As\u00ED es como quedan distribuidos los equipos luego de implementar el Algoritmo");
		labelAlg.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelAlg.setHorizontalAlignment(SwingConstants.CENTER);
		labelAlg.setBounds(0, 245, 841, 33);
		getContentPane().add(labelAlg);
		labelAlg.setVisible(false);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		JButton btnEjecutarAlgoritmo = new JButton(" Ejecutar algoritmo ");
		btnEjecutarAlgoritmo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(primeraVez == false) {
					alg.ordenarGrupos(arrayGruposTemp,arrayGruposFinales);
					primeraVez = true;
				}
				else {
					alg.mejorarRespuesta(arrayGruposTemp,arrayGruposFinales);	
				}
				
				if(dispersionArray(arrayGruposFinales) != dispersionArray(arrayGruposTemp)) {
				arrayGruposTemp = alg.copiarArray(arrayGruposFinales);
				visualizarGrupos(arrayGruposFinales);
				labelAlg.setVisible(true);
				visualizarInfoGrupos(arrayGruposFinales);
				JOptionPane.showMessageDialog(null, " El algoritmo se ejecut� con exito\n el tiempo que tard� en milisegundos fue : " + alg.getTiempoTotal() ,
						" La Copa Am�rica ", JOptionPane.INFORMATION_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(null, " El algoritmo no pudo encontrar un mejor ordenamiento " ,
							" La Copa Am�rica ", JOptionPane.INFORMATION_MESSAGE);
				
			}
		});
		btnEjecutarAlgoritmo.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		btnEjecutarAlgoritmo.setBounds(0, 624, 183, 41);
		this.getContentPane().add(btnEjecutarAlgoritmo);
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		JLabel labelInfoGrupos = new JLabel("Informaci\u00F3n de los Grupos :");
		labelInfoGrupos.setBackground(Color.WHITE);
		labelInfoGrupos.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupos.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupos.setBounds(0, 289, 841, 33);
		getContentPane().add(labelInfoGrupos);
		
		JPanel panelInfoGrupoA = new JPanel();
		panelInfoGrupoA.setBorder(new LineBorder(new Color(255, 0, 0), 4));
		panelInfoGrupoA.setBounds(10, 340, 266, 33);
		getContentPane().add(panelInfoGrupoA);
		panelInfoGrupoA.setLayout(null);
		
		JLabel labelInfoGrupoA = new JLabel("Grupo A");
		labelInfoGrupoA.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoA.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoA.setBounds(0, 0, 266, 33);
		panelInfoGrupoA.add(labelInfoGrupoA);
		
		JPanel panelInfoEquiposA = new JPanel();
		panelInfoEquiposA.setBorder(new LineBorder(Color.WHITE, 2));
		panelInfoEquiposA.setBounds(10, 379, 123, 25);
		getContentPane().add(panelInfoEquiposA);
		panelInfoEquiposA.setLayout(null);
		
		JLabel labelInfoEquiposA = new JLabel("Equipo");
		labelInfoEquiposA.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoEquiposA.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoEquiposA.setBounds(0, 0, 123, 25);
		panelInfoEquiposA.add(labelInfoEquiposA);
		
		JPanel panelInfoCFA = new JPanel();
		panelInfoCFA.setBorder(new LineBorder(Color.WHITE, 2));
		panelInfoCFA.setBounds(153, 379, 123, 25);
		getContentPane().add(panelInfoCFA);
		panelInfoCFA.setLayout(null);
		
		JLabel labelInfoCFA = new JLabel("Coeficiente F");
		labelInfoCFA.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoCFA.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoCFA.setBounds(0, 0, 123, 25);
		panelInfoCFA.add(labelInfoCFA);
		
		labelInfoGrupoAequipo1 = new JLabel("");
		labelInfoGrupoAequipo1.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoAequipo1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoAequipo1.setBounds(10, 415, 123, 19);
		getContentPane().add(labelInfoGrupoAequipo1);
		
		labelInfoGrupoAequipo2 = new JLabel("");
		labelInfoGrupoAequipo2.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoAequipo2.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoAequipo2.setBounds(10, 445, 123, 19);
		getContentPane().add(labelInfoGrupoAequipo2);
		
		labelInfoGrupoAequipo3 = new JLabel("");
		labelInfoGrupoAequipo3.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoAequipo3.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoAequipo3.setBounds(10, 469, 123, 19);
		getContentPane().add(labelInfoGrupoAequipo3);
		
		labelInfoGrupoAequipo4 = new JLabel("");
		labelInfoGrupoAequipo4.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoAequipo4.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoAequipo4.setBounds(10, 494, 123, 19);
		getContentPane().add(labelInfoGrupoAequipo4);
		
		labelInfoGrupoACF1 = new JLabel("");
		labelInfoGrupoACF1.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoACF1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoACF1.setBounds(153, 415, 123, 19);
		getContentPane().add(labelInfoGrupoACF1);
		
		labelInfoGrupoACF2 = new JLabel("");
		labelInfoGrupoACF2.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoACF2.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoACF2.setBounds(153, 447, 123, 17);
		getContentPane().add(labelInfoGrupoACF2);
		
		labelInfoGrupoACF3 = new JLabel("");
		labelInfoGrupoACF3.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoACF3.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoACF3.setBounds(153, 471, 123, 17);
		getContentPane().add(labelInfoGrupoACF3);
		
		labelInfoGrupoACF4 = new JLabel("");
		labelInfoGrupoACF4.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoACF4.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoACF4.setBounds(153, 496, 123, 14);
		getContentPane().add(labelInfoGrupoACF4);
		
		JPanel panelInfoGrupoB = new JPanel();
		panelInfoGrupoB.setBorder(new LineBorder(Color.BLUE, 4));
		panelInfoGrupoB.setBounds(286, 340, 269, 33);
		getContentPane().add(panelInfoGrupoB);
		panelInfoGrupoB.setLayout(null);
		
		JLabel labelInfoGrupoB = new JLabel("Grupo B");
		labelInfoGrupoB.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoB.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoB.setBounds(0, 0, 269, 33);
		panelInfoGrupoB.add(labelInfoGrupoB);
		
		JPanel panelInfoGrupoC = new JPanel();
		panelInfoGrupoC.setBorder(new LineBorder(Color.GREEN, 4));
		panelInfoGrupoC.setBounds(565, 340, 276, 33);
		getContentPane().add(panelInfoGrupoC);
		panelInfoGrupoC.setLayout(null);
		
		JLabel labelInfoGrupoC = new JLabel("Grupo C");
		labelInfoGrupoC.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoC.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoC.setBounds(0, 0, 276, 33);
		panelInfoGrupoC.add(labelInfoGrupoC);
		
		JPanel panelInfoEquiposB = new JPanel();
		panelInfoEquiposB.setBorder(new LineBorder(Color.BLACK, 2));
		panelInfoEquiposB.setBounds(286, 379, 121, 25);
		getContentPane().add(panelInfoEquiposB);
		panelInfoEquiposB.setLayout(null);
		
		JLabel labelInfoEquiposB = new JLabel("Equipo");
		labelInfoEquiposB.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoEquiposB.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoEquiposB.setBounds(0, 0, 121, 25);
		panelInfoEquiposB.add(labelInfoEquiposB);
		
		JPanel panelInfoCFB = new JPanel();
		panelInfoCFB.setBorder(new LineBorder(Color.BLACK, 2));
		panelInfoCFB.setBounds(417, 379, 138, 25);
		getContentPane().add(panelInfoCFB);
		panelInfoCFB.setLayout(null);
		
		JLabel labelInfoCFB = new JLabel("Coeficiente F");
		labelInfoCFB.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoCFB.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoCFB.setBounds(0, 0, 138, 25);
		panelInfoCFB.add(labelInfoCFB);
		
		JPanel panelInfoEquiposC = new JPanel();
		panelInfoEquiposC.setBorder(new LineBorder(Color.GRAY, 2));
		panelInfoEquiposC.setBounds(565, 379, 123, 25);
		getContentPane().add(panelInfoEquiposC);
		panelInfoEquiposC.setLayout(null);
		
		JLabel labelInfoEquiposC = new JLabel("Equipo");
		labelInfoEquiposC.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoEquiposC.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoEquiposC.setBounds(0, 0, 123, 25);
		panelInfoEquiposC.add(labelInfoEquiposC);
		
		JPanel panelInfoCFC = new JPanel();
		panelInfoCFC.setBorder(new LineBorder(Color.GRAY, 2));
		panelInfoCFC.setBounds(698, 379, 143, 25);
		getContentPane().add(panelInfoCFC);
		panelInfoCFC.setLayout(null);
		
		JLabel labelInfoCFC = new JLabel("Coeficiente F");
		labelInfoCFC.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoCFC.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoCFC.setBounds(0, 0, 143, 25);
		panelInfoCFC.add(labelInfoCFC);
		
		labelInfoGrupoBequipo1 = new JLabel("New label");
		labelInfoGrupoBequipo1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoBequipo1.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoBequipo1.setBounds(286, 410, 121, 19);
		getContentPane().add(labelInfoGrupoBequipo1);
		
		labelInfoGrupoBequipo2 = new JLabel("New label");
		labelInfoGrupoBequipo2.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoBequipo2.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoBequipo2.setBounds(286, 440, 121, 19);
		getContentPane().add(labelInfoGrupoBequipo2);
		
		labelInfoGrupoBequipo3 = new JLabel("New label");
		labelInfoGrupoBequipo3.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoBequipo3.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoBequipo3.setBounds(286, 469, 121, 19);
		getContentPane().add(labelInfoGrupoBequipo3);
		
		labelInfoGrupoBequipo4 = new JLabel("New label");
		labelInfoGrupoBequipo4.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoBequipo4.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoBequipo4.setBounds(286, 499, 121, 25);
		getContentPane().add(labelInfoGrupoBequipo4);
		
		labelInfoGrupoBCF1 = new JLabel("New label");
		labelInfoGrupoBCF1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoBCF1.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoBCF1.setBounds(417, 410, 138, 24);
		getContentPane().add(labelInfoGrupoBCF1);
		
		labelInfoGrupoBCF2 = new JLabel("New label");
		labelInfoGrupoBCF2.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoBCF2.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoBCF2.setBounds(417, 440, 138, 19);
		getContentPane().add(labelInfoGrupoBCF2);
		
		labelInfoGrupoBCF3 = new JLabel("New label");
		labelInfoGrupoBCF3.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoBCF3.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoBCF3.setBounds(417, 469, 138, 19);
		getContentPane().add(labelInfoGrupoBCF3);
		
		labelInfoGrupoBCF4 = new JLabel("New label");
		labelInfoGrupoBCF4.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoBCF4.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoBCF4.setBounds(417, 502, 138, 19);
		getContentPane().add(labelInfoGrupoBCF4);
		
		labelInfoGrupoCequipo1 = new JLabel("New label");
		labelInfoGrupoCequipo1.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoCequipo1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoCequipo1.setBounds(565, 415, 123, 19);
		getContentPane().add(labelInfoGrupoCequipo1);
		
		labelInfoGrupoCequipo2 = new JLabel("New label");
		labelInfoGrupoCequipo2.setVerticalAlignment(SwingConstants.CENTER);
		labelInfoGrupoCequipo2.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoCequipo2.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoCequipo2.setEnabled(true);
		labelInfoGrupoCequipo2.setBounds(565, 440, 123, 19);
		getContentPane().add(labelInfoGrupoCequipo2);
		
		labelInfoGrupoCequipo3 = new JLabel("New label");
		labelInfoGrupoCequipo3.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoCequipo3.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoCequipo3.setBounds(565, 469, 123, 19);
		getContentPane().add(labelInfoGrupoCequipo3);
		
		labelInfoGrupoCequipo4 = new JLabel("New label");
		labelInfoGrupoCequipo4.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoCequipo4.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoCequipo4.setBounds(565, 494, 123, 19);
		getContentPane().add(labelInfoGrupoCequipo4);
		
		labelInfoGrupoCCF1 = new JLabel("New label");
		labelInfoGrupoCCF1.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoCCF1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoCCF1.setBounds(698, 415, 133, 19);
		getContentPane().add(labelInfoGrupoCCF1);
		
		labelInfoGrupoCCF2 = new JLabel("New label");
		labelInfoGrupoCCF2.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoCCF2.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoCCF2.setBounds(698, 440, 133, 19);
		getContentPane().add(labelInfoGrupoCCF2);
		
		labelInfoGrupoCCF3 = new JLabel("New label");
		labelInfoGrupoCCF3.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoCCF3.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoCCF3.setBounds(698, 469, 133, 19);
		getContentPane().add(labelInfoGrupoCCF3);
		
		labelInfoGrupoCCF4 = new JLabel("New label");
		labelInfoGrupoCCF4.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoGrupoCCF4.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelInfoGrupoCCF4.setBounds(698, 494, 133, 19);
		getContentPane().add(labelInfoGrupoCCF4);
		
		JLabel lblPromedio_2 = new JLabel("Promedio :");
		lblPromedio_2.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblPromedio_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromedio_2.setBounds(10, 544, 123, 25);
		getContentPane().add(lblPromedio_2);
		
		labelPromGrupoA = new JLabel("New label");
		labelPromGrupoA.setHorizontalAlignment(SwingConstants.CENTER);
		labelPromGrupoA.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelPromGrupoA.setBounds(153, 549, 123, 20);
		getContentPane().add(labelPromGrupoA);
		
		JLabel lblDispersin = new JLabel("Dispersi\u00F3n :");
		lblDispersin.setHorizontalAlignment(SwingConstants.CENTER);
		lblDispersin.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblDispersin.setBounds(10, 580, 123, 20);
		getContentPane().add(lblDispersin);
		
		labelDispGrupoA = new JLabel("New label");
		labelDispGrupoA.setHorizontalAlignment(SwingConstants.CENTER);
		labelDispGrupoA.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelDispGrupoA.setBounds(153, 580, 123, 17);
		getContentPane().add(labelDispGrupoA);
		
		JLabel lblPromedio = new JLabel("Promedio :");
		lblPromedio.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblPromedio.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromedio.setBounds(286, 545, 120, 25);
		getContentPane().add(lblPromedio);
		
		labelPromGrupoB = new JLabel("New label");
		labelPromGrupoB.setHorizontalAlignment(SwingConstants.CENTER);
		labelPromGrupoB.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelPromGrupoB.setBounds(417, 550, 138, 19);
		getContentPane().add(labelPromGrupoB);
		
		JLabel lblNewLabel_3 = new JLabel("Dispersi\u00F3n :");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_3.setBounds(286, 580, 121, 17);
		getContentPane().add(lblNewLabel_3);
		
		labelDispGrupoB = new JLabel("New label");
		labelDispGrupoB.setHorizontalAlignment(SwingConstants.CENTER);
		labelDispGrupoB.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelDispGrupoB.setBounds(417, 578, 138, 19);
		getContentPane().add(labelDispGrupoB);
		
		JLabel lblPromedio_1 = new JLabel("Promedio :");
		lblPromedio_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromedio_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblPromedio_1.setBounds(565, 544, 123, 19);
		getContentPane().add(lblPromedio_1);
		
		labelPromGrupoC = new JLabel("New label");
		labelPromGrupoC.setHorizontalAlignment(SwingConstants.CENTER);
		labelPromGrupoC.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelPromGrupoC.setBounds(698, 544, 133, 19);
		getContentPane().add(labelPromGrupoC);
		
		JLabel lblDispersin_1 = new JLabel("Dispersi\u00F3n :");
		lblDispersin_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblDispersin_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblDispersin_1.setBounds(565, 578, 123, 19);
		getContentPane().add(lblDispersin_1);
		
		labelDispGrupoC = new JLabel("New label");
		labelDispGrupoC.setHorizontalAlignment(SwingConstants.CENTER);
		labelDispGrupoC.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		labelDispGrupoC.setBounds(698, 580, 133, 17);
		getContentPane().add(labelDispGrupoC);
		
		visualizarGrupos(arrayGruposFinales);
		visualizarInfoGrupos(arrayGruposFinales);
		
	}

	private void visualizarInfoGrupos(ArrayList<Grupo> arrayGruposFinales2) {
		//-----------------Grupo A : 
				labelInfoGrupoAequipo1.setText(arrayGruposFinales2.get(0).getListaEquipos().get(0).getNombre());
				labelInfoGrupoAequipo2.setText(arrayGruposFinales2.get(0).getListaEquipos().get(1).getNombre());
				labelInfoGrupoAequipo3.setText(arrayGruposFinales2.get(0).getListaEquipos().get(2).getNombre());
				labelInfoGrupoAequipo4.setText(arrayGruposFinales2.get(0).getListaEquipos().get(3).getNombre());
				
				labelInfoGrupoACF1.setText(arrayGruposFinales2.get(0).getListaEquipos().get(0).getFuerza() +"");
				labelInfoGrupoACF2.setText(arrayGruposFinales2.get(0).getListaEquipos().get(1).getFuerza() +"");
				labelInfoGrupoACF3.setText(arrayGruposFinales2.get(0).getListaEquipos().get(2).getFuerza() +"");
				labelInfoGrupoACF4.setText(arrayGruposFinales2.get(0).getListaEquipos().get(3).getFuerza() +"");
				
				labelPromGrupoA.setText(arrayGruposFinales2.get(0).promedio()+"");
				labelDispGrupoA.setText(arrayGruposFinales2.get(0).dispersionGrupo()+"");
	   //-------------------Grupo B :
				labelInfoGrupoBequipo1.setText(arrayGruposFinales2.get(1).getListaEquipos().get(0).getNombre());
				labelInfoGrupoBequipo2.setText(arrayGruposFinales2.get(1).getListaEquipos().get(1).getNombre());
				labelInfoGrupoBequipo3.setText(arrayGruposFinales2.get(1).getListaEquipos().get(2).getNombre());
				labelInfoGrupoBequipo4.setText(arrayGruposFinales2.get(1).getListaEquipos().get(3).getNombre());
				
				labelInfoGrupoBCF1.setText(arrayGruposFinales2.get(1).getListaEquipos().get(0).getFuerza() +"");
				labelInfoGrupoBCF2.setText(arrayGruposFinales2.get(1).getListaEquipos().get(1).getFuerza() +"");
				labelInfoGrupoBCF3.setText(arrayGruposFinales2.get(1).getListaEquipos().get(2).getFuerza() +"");
				labelInfoGrupoBCF4.setText(arrayGruposFinales2.get(1).getListaEquipos().get(3).getFuerza() +"");
				
				labelPromGrupoB.setText(arrayGruposFinales2.get(1).promedio()+"");
				labelDispGrupoB.setText(arrayGruposFinales2.get(1).dispersionGrupo()+"");
	//---------------------Grupo C :
				labelInfoGrupoCequipo1.setText(arrayGruposFinales2.get(2).getListaEquipos().get(0).getNombre());
				labelInfoGrupoCequipo2.setText(arrayGruposFinales2.get(2).getListaEquipos().get(1).getNombre());
				labelInfoGrupoCequipo3.setText(arrayGruposFinales2.get(2).getListaEquipos().get(2).getNombre());
				labelInfoGrupoCequipo4.setText(arrayGruposFinales2.get(2).getListaEquipos().get(3).getNombre());
				
				labelInfoGrupoCCF1.setText(arrayGruposFinales2.get(2).getListaEquipos().get(0).getFuerza() +"");
				labelInfoGrupoCCF2.setText(arrayGruposFinales2.get(2).getListaEquipos().get(1).getFuerza() +"");
				labelInfoGrupoCCF3.setText(arrayGruposFinales2.get(2).getListaEquipos().get(2).getFuerza() +"");
				labelInfoGrupoCCF4.setText(arrayGruposFinales2.get(2).getListaEquipos().get(3).getFuerza() +"");
				
				labelPromGrupoC.setText(arrayGruposFinales2.get(2).promedio()+"");
				labelDispGrupoC.setText(arrayGruposFinales2.get(2).dispersionGrupo()+"");

	}

	private void visualizarGrupos(ArrayList<Grupo> arrayGrupos2) {
		
		labelGrupoBUno.setText(arrayGrupos2.get(0).getListaEquipos().get(0).getNombre());
		labelGrupoBDos.setText(arrayGrupos2.get(0).getListaEquipos().get(1).getNombre());
		labelGrupoBTres.setText(arrayGrupos2.get(0).getListaEquipos().get(2).getNombre());
		labelGrupoBCuatro.setText(arrayGrupos2.get(0).getListaEquipos().get(3).getNombre());
		
		labelGrupoAUno.setText(arrayGrupos2.get(1).getListaEquipos().get(0).getNombre());
		labelGrupoADos.setText(arrayGrupos2.get(1).getListaEquipos().get(1).getNombre());
		labelGrupoATres.setText(arrayGrupos2.get(1).getListaEquipos().get(2).getNombre());
		labelGrupoACuatro.setText(arrayGrupos2.get(1).getListaEquipos().get(3).getNombre());
		
		labelGrupoCUno.setText(arrayGrupos2.get(2).getListaEquipos().get(0).getNombre());
		labelGrupoCDos.setText(arrayGrupos2.get(2).getListaEquipos().get(1).getNombre());
		labelGrupoCTres.setText(arrayGrupos2.get(2).getListaEquipos().get(2).getNombre());
		labelGrupoCCuatro.setText(arrayGrupos2.get(2).getListaEquipos().get(3).getNombre());
	}

	private void agregarGrupos(ArrayList<Grupo> grupos) {

		ArrayList<Equipo> g1 = new ArrayList<Equipo>();
		ArrayList<Equipo> g2 = new ArrayList<Equipo>();
		ArrayList<Equipo> g3 = new ArrayList<Equipo>();
		
		g1.add(new Equipo("Argentina", 41));
		g1.add(new Equipo("Bolivia", 26));
		g1.add(new Equipo("Brasil", 35));
		g1.add(new Equipo("Chile", 38));
		
		g2.add(new Equipo("Colombia", 21));
		g2.add(new Equipo("Costa Rica", 5));
		g2.add(new Equipo("Ecuador", 27));
		g2.add(new Equipo("Estados Unidos", 4));
		
		g3.add(new Equipo("Mexico", 10));
		g3.add(new Equipo("Paraguay", 36));
		g3.add(new Equipo("Peru", 31));
		g3.add(new Equipo("Venezuela", 27));

		Grupo grupoA = new Grupo("grupoA", g1);
		Grupo grupoB = new Grupo("grupoB", g2);
		Grupo grupoC = new Grupo("grupoC", g3);

		grupos.add(grupoA);
		grupos.add(grupoB);
		grupos.add(grupoC);
	}
	
	public int dispersionArray(ArrayList<Grupo> grupos) {
		int ret=0;
		for (Grupo grupo : grupos) {
			ret+=grupo.dispersionGrupo();
		}
		return ret;
	}

	public boolean getPrimeraVez() {
		return primeraVez;
	}

	public void setPrimeraVez(boolean primeraVez) {
		this.primeraVez = primeraVez;
	}
}
